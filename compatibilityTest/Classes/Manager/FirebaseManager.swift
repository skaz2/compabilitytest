//
//  FirebaseManager.swift
//  compatibilityTest
//
//  Created by spbiphones on 26.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation
import Firebase

class FirebaseManager {
	
	var sourceRef: DatabaseReference {
			return Database.database().reference()
	}
	
	var queueRef: DatabaseReference {
		return sourceRef.child("queue")
	}
	
	var sessionsRef: DatabaseReference {
		return sourceRef.child("sessions")
	}
	
}
