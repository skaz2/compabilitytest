//
//  AuthManager.swift
//  compatibilityTest
//
//  Created by spbiphones on 26.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class AuthManager: FirebaseManager {
	var currentUser: User?
	
	static let shared = AuthManager()
	
	private let auth = Auth.auth()
	private var registerModel = RegisterModel()
	public private(set) var code = ""
	public private(set) var id: String {
		get {
			if let id = UserDefaults.standard.string(forKey: "id") {
				return id
			} else {
				let uid = UUID().uuidString
				self.id = uid
				return uid
			}
			
		}
		set {
			UserDefaults.standard.set(newValue, forKey: "id")
			UserDefaults.standard.synchronize()
		}
	}
	
	
	public private(set) var sessionCode: String {
		get {
			return UserDefaults.standard.string(forKey: "sessionCode") ?? ""
		}
		set {
			UserDefaults.standard.set(newValue, forKey: "sessionCode")
			UserDefaults.standard.synchronize()
		}
	}
	var sessionId: String?
	var countOfQuestions: Int = 60

	
	func queue() {
		self.queueRef.child(id).setValue(code)
	}
	
	func moveToSession(partnerCode: String, complition: @escaping ItemClosure<Bool>) {
		self.queueRef.observeSingleEvent(of: .value) { (snapshot) in
		
			if let dict = snapshot.value as? [String: String] {
				var isFinded = false
				var findedId: String?
				for element in dict where element.value == partnerCode {
					findedId = element.key
					isFinded = true
				}
				
				if isFinded {
					let sessionCode = "\(self.code)_\(partnerCode)"
					self.queueRef.child(self.id).removeValue()
					self.queueRef.child(findedId!).removeValue()
					self.sessionsRef.child(sessionCode).child("user2").setValue(123)
					self.sessionCode = sessionCode
				}
				
				complition(isFinded)
			}
		}
	}
	
	func finishTest(answers: [[Int]], completion: (() -> Void)?) {
		self.sessionsRef.child(sessionCode).child("answers").child(id).setValue(answers) { (_, _) in
			
		}
	}
	
	//	func observeSessions(callback: (([String: Any]) -> Void)? = nil) {
	func observeSessions(startObserve: Bool, complition: @escaping ItemClosure<String?>) {
		if startObserve {
			self.sessionsRef.observe(.value) { (snapshot) in
				if let dict = snapshot.value as? [String: [String: Any]] {
					
					for element in dict where element.key.contains(self.code) {
						// finded
						if ((element.value["isFinished"] as? Bool) ?? false) == false {
							self.sessionsRef.child(element.key).child("user1").setValue(231)
							self.sessionCode = element.key
							complition(element.key)
							return
						}	
					}
					complition(nil)
					return
				}
			}
		}
	}
	
	func observeSession(completion: @escaping ([String: [[Int]]]) -> Void) {
		self.sessionsRef.child(sessionCode).child("answers").observe(.value) { (snapshot) in
			if let dictionary = snapshot.value as? [String: Any], dictionary.count == 2 {
				self.sessionsRef.child(self.sessionCode).child("isFinished").setValue(true) { (_, _) in
					self.sessionsRef.child(self.sessionCode).removeValue(completionBlock: { (_, _) in
						completion(dictionary as! [String: [[Int]]])
					})
				}
			}
		}
	}
	
	func codeIfNeeded() {
		if let getCode = UserDefaults.standard.string(forKey: "Code") {
			code = getCode
		} else {
			code = generateCode()
			UserDefaults.standard.set(code, forKey: "Code")
		}
	}
}
