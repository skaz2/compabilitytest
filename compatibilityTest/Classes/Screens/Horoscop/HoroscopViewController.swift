//
//  HoroscopViewController.swift
//  compatibilityTest
//
//  Created by spbiphones on 27.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

class HoroscopViewController: UIViewController, XMLParserDelegate {
	
	@IBOutlet weak var tableView: UITableView!
	
	struct Zodiac {
		var zodiacTitle: String
		var zodiacText: String
	}
	
	var parser: XMLParser!
	var zodiacs: [Zodiac] = []
	var elementName: String = String()
	var zodiacTitle = String()
	var zodiacText = String()
	let horoscopeURL = URL(string: HOROSCOPEURL)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		Decorator.decorate(self)
		delegating()
		parser = XMLParser(contentsOf: horoscopeURL!)
		parser.delegate = self
		parser.parse()
		registerCells()
		title = "Гороскоп"
		
	}
	
	private func delegating() {
		tableView.delegate = self
		tableView.dataSource = self
		
		
	}
	
	private func registerCells() {
		tableView.register(ZodiacTableViewCell.nib, forCellReuseIdentifier: ZodiacTableViewCell.name)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
		
		if elementName == "aries" {
			zodiacTitle = "Овен"
			zodiacText = String()
		} else if elementName == "taurus" {
			zodiacTitle = "Телец"
			zodiacText = String()
		} else if elementName == "gemini" {
			zodiacTitle = "Близнецы"
			zodiacText = String()
		} else if elementName == "cancer" {
			zodiacTitle = "Рак"
			zodiacText = String()
		} else if elementName == "leo" {
			zodiacTitle = "Лев"
			zodiacText = String()
		} else if elementName == "virgo" {
			zodiacTitle = "Дева"
			zodiacText = String()
		} else if elementName == "libra" {
			zodiacTitle = "Весы"
			zodiacText = String()
		} else if elementName == "scorpio" {
			zodiacTitle = "Скорпион"
			zodiacText = String()
		} else if elementName == "sagittarius" {
			zodiacTitle = "Стрелец"
			zodiacText = String()
		} else if elementName == "capricorn" {
			zodiacTitle = "Козерог"
			zodiacText = String()
		} else if elementName == "aquarius" {
			zodiacTitle = "Водолей"
			zodiacText = String()
		} else if elementName == "pisces" {
			zodiacTitle = "Рыбы"
			zodiacText = String()
		}
		
		
		self.elementName = elementName
	}
	
	
	func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
		if elementName == "aries" || elementName == "taurus" || elementName == "gemini" || elementName == "cancer" || elementName == "leo" || elementName == "virgo" ||  elementName == "libra" || elementName == "scorpio" || elementName == "sagittarius"  || elementName == "capricorn" || elementName == "aquarius" || elementName == "pisces"{
			let zodiac = Zodiac(zodiacTitle: zodiacTitle, zodiacText: zodiacText)
			zodiacs.append(zodiac)
			
		}
		
		tableView.reloadData()
	}
	
	
	func parser(_ parser: XMLParser, foundCharacters string: String) {
		let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		
		if (!data.isEmpty) {
			if self.elementName == "aries" {
				zodiacTitle += "Овен"
			} else if self.elementName == "taurus" {
				zodiacTitle += "Телец"
			} else if self.elementName == "today" {
				zodiacText += data
			}
		}
	}
}


extension HoroscopViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 150
	}
}

extension HoroscopViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: ZodiacTableViewCell.name, for: indexPath) as! ZodiacTableViewCell
		let zodiac = zodiacs[indexPath.row]
		cell.titleLabel.text = zodiac.zodiacTitle
		cell.zodiacTextLabel.text = zodiac.zodiacText
		print(zodiac.zodiacTitle)
		return cell
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return zodiacs.count
	}
}

extension HoroscopViewController {
	fileprivate class Decorator {
		private init() {}
		static func decorate(_ vc: HoroscopViewController) {
			vc.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
			vc.navigationController?.navigationBar.isTranslucent = false
			vc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
			vc.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
		}
	}
}
