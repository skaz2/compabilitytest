//
//  FirstTextVC.swift
//  compatibilityTest
//
//  Created by spbiphones on 17.01.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var nextButton: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		addTargets()
		
	}
	
	private func addTargets() {
		nextButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
	}
	
	@objc private func nextButtonPressed() {
		StartRouter.shared.goToVC(from: self)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
}

extension ViewController {
	fileprivate class Decorator {
		static let buttonCornerRadius: CGFloat = 5
		private init() {}
		static func decorate(_ vc: ViewController) {
			
			//vc.signInBtn.view.clipToBounds = true
			
			
		}
	}
}

