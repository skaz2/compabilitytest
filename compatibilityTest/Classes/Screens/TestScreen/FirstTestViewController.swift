//
//  OneTestViewController.swift
//  compatibilityTest
//
//  Created by spbiphones on 09.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

class FirstTestViewController: UIViewController {
	
	@IBOutlet weak var menuBtn: UIButton!
	@IBOutlet weak var beginTestBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        addTargets()
    }


	private func addTargets() {
		beginTestBtn.addTarget(self, action: #selector(beginTestBtnPressed), for: .touchUpInside)
	}
	
	@objc private func menuBtnPressed() {
		StartRouter.shared.goToVC(from: self)
	}
	
	@objc private func beginTestBtnPressed() {
		StartRouter.shared.goToTestVC(from: self)
	}

}
