//
//  ViewController.swift
//  compatibilityTest
//
//  Created by spbiphones on 17.01.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth



class ViewController2: UIViewController, UITextFieldDelegate {
	
	@IBOutlet weak var testOneButton: UIButton!
	@IBOutlet weak var testTwoButton: UIButton!
	@IBOutlet weak var testThreeButton: UIButton!
	@IBOutlet weak var horoskopButton: UIButton!
	@IBOutlet weak var ownCodeLabel: UILabel!
	@IBOutlet weak var enterCodeTextField: UITextField!
	@IBOutlet weak var shareCodeButton: UIButton!
	
	
	private var partnerReady = false
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
		view.addGestureRecognizer(tap)
		addTargets()
		view.bindToKeyboard()
		testIsInactive()
		ownCodeLabel.text = "Ваш код: \(AuthManager.shared.code)"
		AuthManager.shared.queue()
		AuthManager.shared.observeSessions(startObserve: true) { (session) in
			if session != nil {
				AuthManager.shared.sessionId = session
				let vc = Test1ViewController()
				
				self.present(vc, animated: true, completion: nil)
				self.testsIsActive()
				AuthManager.shared.observeSession(completion: { (answers) in
					vc.dismiss(animated: true, completion: nil)
					let values = answers.values
					var firstAnswers: [[Int]] = []
					var secondAnswers: [[Int]] = []
					values.enumerated().forEach({ (index, element) in
						if index == 0 {
							firstAnswers = element
						} else {
							secondAnswers = element
						}
					})
					
					for i in 0..<firstAnswers.count {
						let oneAnswers = firstAnswers[i] // ответы первого человека (может быть 1 или 2)
						let twoAnswers = secondAnswers[i] // ответы второго человека (может быть 1 или 2)
						let countOfGeneralAnswers = oneAnswers.countOfEqualElements(with: twoAnswers)
						//						2 * 60 / 2
						let percentage = Int(Double(countOfGeneralAnswers) / Double(AuthManager.shared.countOfQuestions) * 100)
						let alertController = UIAlertController.init(title: "Тест закончен", message: "У вас совпадения в \(percentage)%", preferredStyle: .alert)
						print("У вас совпадения в \(percentage)")
						alertController.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
						self.present(alertController, animated: true, completion: nil)
					}
				})
			}
		}
		enterCodeTextField.delegate = self
		NotificationCenter.default
			.addObserver(self,
									 selector: #selector(statusManager),
									 name: .flagsChanged,
									 object: nil)
		updateUserInterface()
	}
	
	func updateUserInterface() {
		if !Network.reachability.isReachable {
			self.showAlert(with: "Ошибка", and: "Нет доступа к интернету")
		}
	}
	@objc func statusManager(_ notification: Notification) {
		updateUserInterface()
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		guard let code = textField.text, !code.isEmpty else {
			return true
		}
		
		AuthManager.shared.moveToSession(partnerCode: code) { (result) in
			
		}
		
		
		return true
	}
	
	@objc func handleTap() {
		view.endEditing(true)
	}
	
	private func testsIsActive() {
		//		tests.layer.opacity = 1
		//		testsTwoBg.layer.opacity = 1
		//		testThreeBg.layer.opacity = 1
		partnerReady = true
	}
	
	private func testIsInactive() {
		//		tests.layer.opacity = 0.75
		//		testsTwoBg.layer.opacity = 0.75
		//		testThreeBg.layer.opacity = 0.75
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(true, animated: true)
		
	}
	
	private func addTargets() {
		testOneButton.addTarget(self, action: #selector(testOneButtonPressed), for: .touchUpInside)
		testTwoButton.addTarget(self, action: #selector(testTwoButtonPressed), for: .touchUpInside)
		testThreeButton.addTarget(self, action: #selector(testThreeButtonPressed), for: .touchUpInside)
		horoskopButton.addTarget(self, action: #selector(horoskopButtonPressed), for: .touchUpInside)
		shareCodeButton.addTarget(self, action: #selector(shareCodeButtonPressed), for: .touchUpInside)
	}
	
	@objc private func shareCodeButtonPressed() {
		let invitationCode = AuthManager.shared.code
		print(invitationCode)
		UIPasteboard.general.string = invitationCode
		self.share(invitationCode: invitationCode)
	}
	
	func share(invitationCode: String) {
		
		UIGraphicsBeginImageContext(view.frame.size)
		view.layer.render(in: UIGraphicsGetCurrentContext()!)
		//			let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		
		let objectsToShare = [invitationCode] as [Any]
		let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
		
		//Excluded Activities
		activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
		//
		
		//	activityVC.popoverPresentationController?.sourceView = sender
		self.present(activityVC, animated: true, completion: nil)
	}
	
	private func wait() {
		self.showAlert(with: "Ждем", and: "Партнер еще не вошел")
	}
	
	@objc private func testOneButtonPressed() {
		StartRouter.shared.goToTestVC(from: self)
		//		if partnerReady {
		//		StartRouter.shared.goToFirstTextVC(from: self)
		//		} else {
		//			self.wait()
		//		}
	}
	
	@objc private func testTwoButtonPressed() {
		if partnerReady {
			//	StartRouter.shared.goToFirstTextVC(from: self)
		} else {
			self.wait()
		}
	}
	
	@objc private func testThreeButtonPressed() {
		if partnerReady {
			//	StartRouter.shared.goToFirstTextVC(from: self)
		} else {
			self.wait()
		}
	}
	
	@objc private func horoskopButtonPressed() {
		StartRouter.shared.goToHoroscopeVC(from: self)
	}
	
	
}



