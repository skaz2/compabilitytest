//
//  Test1ViewController.swift
//  compatibilityTest
//
//  Created by Hadevs on 04/04/2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit
import Firebase

class TestResults {
	static var answers: [[Int]] = []
}

class Test1ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var questionLabel: UILabel!
	
	var questionIndex: Int = 0
	private var answers: [String] {
		return [questionModel.question1, questionModel.question2, questionModel.question3 , questionModel.question4, questionModel.question5, questionModel.question6].filter { !$0.isEmpty }
	}
	
	private var questionModel: Test1QuestionsModel {
		return Test1QuestionsModel.fetchTest1()[questionIndex]
	}
	
	var finishClosure: (() -> Void)?
	private var selectedAnswers: [Int] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupTableView()
		setTitle()
		registerCells()
		addBottomButton()
	}
	
	private func registerCells() {
		tableView.register(AnswerTableViewCell.nib, forCellReuseIdentifier: AnswerTableViewCell.name)
	}
	
	private func setTitle() {
		let count = Test1TitleModel.fetchTest1().count
		AuthManager.shared.countOfQuestions = count
		titleLabel.text = "\(questionIndex + 1) из \(count)"
	}
	
	private func setupTableView() {
		tableView.delegate = self
		tableView.dataSource = self
		tableView.separatorColor = .clear
	}
	
	private func addBottomButton() {
		let buttonView = DoneButtonView.loadFromNib()
		let buttonHeight: CGFloat = 44
		let topPadding: CGFloat = 32
		buttonView.frame.size = CGSize(width: tableView.frame.width, height: buttonHeight + topPadding)
		buttonView.buttonClicked = doneButtonClicked
		tableView.tableFooterView = buttonView
	}
	
	private func doneButtonClicked() {
		let count = Test1TitleModel.fetchTest1().count
		if questionIndex == count - 1 {
			// last
			finishTest()
		} else {
			next()
		}
	}
	
	private func finishTest() {
		addAnswer()
		AuthManager.shared.finishTest(answers: TestResults.answers, completion: {
			
		})
		
	}
	
	private func addAnswer() {
		if TestResults.answers.indices.contains(questionIndex) {
			TestResults.answers[questionIndex] = selectedAnswers
		} else {
			TestResults.answers.insert(selectedAnswers, at: questionIndex)
		}
	}
	
	private func next() {
		// 1. Сохранить ответы
		addAnswer()
		
		// 2. Перейти на следующий экран
		questionIndex += 1
		tableView.reloadData()
		setTitle()
		clearCells()
	}
	
	private func clearCells() {
		tableView.visibleCells.forEach { (sourceCell) in
			if let cell = sourceCell as? AnswerTableViewCell {
				cell.button.setImage(UIImage(named: "btn"), for: .normal)
			}
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: AnswerTableViewCell.name, for: indexPath) as! AnswerTableViewCell
		cell.configure(by: answers[indexPath.section])
		cell.buttonSelected = { result in
			self.answerClicked(with: result, with: indexPath.section)
		}
		return cell
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return UIView()
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 44
	}
	
	private func answerClicked(with result: Bool, with index: Int) {
		if result {
			// add
			if !selectedAnswers.contains(index) {
				selectedAnswers.append(index)
			}
			//
		} else {
			// remove
			if let findedIndex = selectedAnswers.index(of: index) {
				selectedAnswers.remove(at: findedIndex)
			}
			//
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 44
	}
	func numberOfSections(in tableView: UITableView) -> Int {
		return answers.count
	}
	
}
