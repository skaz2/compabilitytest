//
//  TestViewController.swift
//  compatibilityTest
//
//  Created by spbiphones on 09.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
	@IBOutlet weak var nextButton: UIButton!
	@IBOutlet weak var questionNumber: UILabel!
	@IBOutlet weak var testTitleLabel: UILabel!
	@IBOutlet weak var menuButton: UIButton!
	@IBOutlet weak var button1: UIButton!
	@IBOutlet weak var titleQuestion1: UILabel!
	@IBOutlet weak var viewQuestion1: UIView!
	@IBOutlet weak var viewQuestion2: UIView!
	@IBOutlet weak var viewQuestion3: UIView!
	@IBOutlet weak var viewQuestion4: UIView!
	@IBOutlet weak var viewQuestion5: UIView!
	@IBOutlet weak var viewQuestion6: UIView!
	@IBOutlet weak var button2: UIButton!
	@IBOutlet weak var titleQuestion2: UILabel!
	@IBOutlet weak var button3: UIButton!
	@IBOutlet weak var titleQuestion3: UILabel!
	@IBOutlet weak var button4: UIButton!
	@IBOutlet weak var titleQuestion4: UILabel!
	@IBOutlet weak var button5: UIButton!
	@IBOutlet weak var titleQuestion5: UILabel!
	@IBOutlet weak var button6: UIButton!
	@IBOutlet weak var titleQuestion6: UILabel!
	@IBOutlet weak var scrollView: UIScrollView!
	
	var id = 0
	var button1active = false
	var button2active = false
	var button3active = false
	var button4active = false
	var button5active = false
	var button6active = false
	var nextAvailable = 0
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		addTargets()
		questionNumber.text = "\(id + 1) из \(Test1TitleModel.fetchTest1().count)"
		viewQuestion1.isHidden = true
		viewQuestion2.isHidden = true
		viewQuestion3.isHidden = true
		viewQuestion4.isHidden = true
		viewQuestion5.isHidden = true
		viewQuestion6.isHidden = true
		load()
	}
	
	private func load() {
		if Test1QuestionsModel.fetchTest1()[id].question1 != "" {
			viewQuestion1.isHidden = false
			titleQuestion1.text = Test1QuestionsModel.fetchTest1()[id].question1
		}
		
		if Test1QuestionsModel.fetchTest1()[id].question2 != "" {
			viewQuestion2.isHidden = false
			titleQuestion2.text = Test1QuestionsModel.fetchTest1()[id].question2
		}
		
		if Test1QuestionsModel.fetchTest1()[id].question3 != "" {
			viewQuestion3.isHidden = false
			titleQuestion3.text = Test1QuestionsModel.fetchTest1()[id].question3
		}
		
		if Test1QuestionsModel.fetchTest1()[id].question4 != "" {
			viewQuestion4.isHidden = false
			titleQuestion4.text = Test1QuestionsModel.fetchTest1()[id].question4
		}
		
		if Test1QuestionsModel.fetchTest1()[id].question5 != "" {
			viewQuestion5.isHidden = false
			titleQuestion5.text = Test1QuestionsModel.fetchTest1()[id].question5
		}
		
		if Test1QuestionsModel.fetchTest1()[id].question6 != "" {
			viewQuestion6.isHidden = false
			titleQuestion6.text = Test1QuestionsModel.fetchTest1()[id].question6
		}
	}
	
	func addTargets() {
		nextButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
		menuButton.addTarget(self, action: #selector(menuButtonPressed), for: .touchUpInside)
		button1.addTarget(self, action: #selector(button1Pressed), for: .touchUpInside)
		button2.addTarget(self, action: #selector(button2Pressed), for: .touchUpInside)
		button3.addTarget(self, action: #selector(button3Pressed), for: .touchUpInside)
		button4.addTarget(self, action: #selector(button4Pressed), for: .touchUpInside)
		button5.addTarget(self, action: #selector(button5Pressed), for: .touchUpInside)
		button6.addTarget(self, action: #selector(button6Pressed), for: .touchUpInside)
	}
	
	@objc private func button1Pressed() {
		
		if button1active {
			button1.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
			button1active = false
			nextAvailable = nextAvailable - 1
		} else {
			button1.setImage(#imageLiteral(resourceName: "okbtn"), for: .normal)
			button1active = true
			nextAvailable = nextAvailable + 1
		}
		
		
	}
	
	@objc private func button2Pressed() {
		if button2active {
			button2.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
			button2active = false
			nextAvailable = nextAvailable - 1
		} else {
			button2.setImage(#imageLiteral(resourceName: "okbtn"), for: .normal)
			button2active = true
			nextAvailable = nextAvailable + 1
		}
	}
	
	@objc private func button3Pressed() {
		if button3active {
			button3.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
			button3active = false
			nextAvailable = nextAvailable - 1
		} else {
			button3.setImage(#imageLiteral(resourceName: "okbtn"), for: .normal)
			button3active = true
			nextAvailable = nextAvailable + 1
		}
	}
	
	@objc private func button4Pressed() {
		if button4active {
			button4.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
			button4active = false
			nextAvailable = nextAvailable - 1
		} else {
			button4.setImage(#imageLiteral(resourceName: "okbtn"), for: .normal)
			button4active = true
			nextAvailable = nextAvailable + 1
		}
	}
	
	@objc private func button5Pressed() {
		if button5active {
			button5.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
			button5active = false
			nextAvailable = nextAvailable - 1
		} else {
			button5.setImage(#imageLiteral(resourceName: "okbtn"), for: .normal)
			button5active = true
			nextAvailable = nextAvailable + 1
		}
	}
	
	@objc private func button6Pressed() {
		if button6active {
			button6.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
			button6active = false
			nextAvailable = nextAvailable - 1
		} else {
			button6.setImage(#imageLiteral(resourceName: "okbtn"), for: .normal)
			button6active = true
			nextAvailable = nextAvailable + 1
		}
	}
	
	@objc func nextButtonPressed() {
		if nextAvailable > 0 && nextAvailable < 3 {
		id = id + 1
		questionNumber.text = "\((id + 1)) из \(Test1TitleModel.fetchTest1().count)"
		testTitleLabel.text = Test1TitleModel.fetchTest1()[id].testLabel
		viewQuestion1.isHidden = true
		viewQuestion2.isHidden = true
		viewQuestion3.isHidden = true
		viewQuestion4.isHidden = true
		viewQuestion5.isHidden = true
		viewQuestion6.isHidden = true
		button1active = false
		button2active = false
		button3active = false
		button4active = false
		button5active = false
		button6active = false
		button1.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
		button2.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
		button3.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
		button4.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
		button5.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
		button6.setImage(#imageLiteral(resourceName: "btn"), for: .normal)
		load()
		self.view.layoutIfNeeded()
		scrollView.scrollToTop(animated: true)
		} else {
			showAlert(with: "Ошибка", and: "Выберите один или два ответа")
		}
	}
	
	@objc func menuButtonPressed() {
		StartRouter.shared.goToVC(from: self)
	}
	
	func share(invitationCode: String) {
		
		UIGraphicsBeginImageContext(view.frame.size)
		view.layer.render(in: UIGraphicsGetCurrentContext()!)
		//			let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		
		let objectsToShare = [invitationCode] as [Any]
		let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
		
		//Excluded Activities
		activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
		//
		
		//	activityVC.popoverPresentationController?.sourceView = sender
		self.present(activityVC, animated: true, completion: nil)
	}
}

extension UIScrollView {
	
	// Scroll to a specific view so that it's top is at the top our scrollview
	func scrollToView(view:UIView, animated: Bool) {
		if let origin = view.superview {
			// Get the Y position of your child view
			let childStartPoint = origin.convert(view.frame.origin, to: self)
			// Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
			self.scrollRectToVisible(CGRect(x:0, y:childStartPoint.y,width: 1,height: self.frame.height), animated: animated)
		}
	}
	
	// Bonus: Scroll to top
	func scrollToTop(animated: Bool) {
		let topOffset = CGPoint(x: 0, y: -contentInset.top)
		setContentOffset(topOffset, animated: animated)
	}
	
	// Bonus: Scroll to bottom
	func scrollToBottom() {
		let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
		if(bottomOffset.y > 0) {
			setContentOffset(bottomOffset, animated: true)
		}
	}
	
}


