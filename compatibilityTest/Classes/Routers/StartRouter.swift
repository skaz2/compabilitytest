//
//  StartRouter.swift
//  compatibilityTest
//
//  Created by spbiphones on 26.01.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

final class StartRouter {
	static let shared = StartRouter()
	
	private init() {}	
	
//	func goToFirstTextVC(from source: UIViewController) {
//		//let vc = FirstTextVC()
//		source.navigationController?.pushViewController(vc, animated: true)
//	}
	
	func goToHoroscopeVC(from source: UIViewController) {
		let vc = HoroscopViewController()
		source.navigationController?.pushViewController(vc, animated: true)
	}
	
	func goToTestVC(from source: UIViewController) {
		let vc = Test1ViewController()
		source.navigationController?.pushViewController(vc, animated: true)
	}
	
	func goToFirstTestVC(from source: UIViewController) {
		let vc = FirstTestViewController()
		source.navigationController?.pushViewController(vc, animated: true)
	}
	
	func goToVC(from source: UIViewController) {
		let vc = ViewController2()
		source.navigationController?.pushViewController(vc, animated: true)
	}
	
	func routeAfterSuccessAuth(from source: UIViewController) {
//		let vc = Router.shared.startControllerAfterAuth
//		source.present(vc, animated: true, completion: nil)
	}
	
	
}
