//
//  AnswerTableViewCell.swift
//  compatibilityTest
//
//  Created by Hadevs on 04/04/2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

class AnswerTableViewCell: UITableViewCell, NibLoadable {
	
	@IBOutlet weak var button: UIButton!
	@IBOutlet weak var label: UILabel!
	
	var buttonSelected: ((Bool) -> Void)?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		backgroundColor = .clear
		selectionStyle = .none
		button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
	}
	
	@objc private func buttonAction() {
		let selectedImage = UIImage(named: "okbtn")!
		let deselectedImage = UIImage(named: "btn")!
		if button.imageView?.image == deselectedImage {
			button.setImage(selectedImage, for: .normal)
			buttonSelected?(true)
		} else {
			button.setImage(deselectedImage, for: .normal)
			buttonSelected?(false)
		}
	}
	
	func configure(by string: String) {
		label.text = string
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
}
