//
//  DoneButtonView.swift
//  compatibilityTest
//
//  Created by Hadevs on 04/04/2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

class DoneButtonView: UIView, NibLoadable {
	@IBOutlet weak var button: UIButton!
	
	var buttonClicked: (() -> Void)?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		backgroundColor = .clear
		button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
	}
	
	@objc private func buttonAction() {
		buttonClicked?()
	}
}
