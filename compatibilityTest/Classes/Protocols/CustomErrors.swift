//
//  CustomErrors.swift
//  compatibilityTest
//
//  Created by spbiphones on 26.01.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation

enum Result<Value> {
	case success(Value)
	case failure(Error)
}

enum CustomErrors {
	case invalidEmail
	case unknownError
	case serverError
	case keychainError
}

extension CustomErrors: LocalizedError {
	var errorDescription: String? {
		switch self {
		case .invalidEmail:
			return NSLocalizedString("Неправильно указан email", comment: "")
		case .unknownError:
			return NSLocalizedString("Неизвестная ошибка", comment: "")
		case .serverError:
			return NSLocalizedString("Ошибка сервера", comment: "")
		case .keychainError:
			return NSLocalizedString("keycahin_error", comment: "")
		}
	}
}
