//
//  Typealiases.swift
//  compatibilityTest
//
//  Created by spbiphones on 26.01.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation

typealias ItemClosure<T> = ((T) -> Void)
typealias OptionalItemClosure<T> = ((T?) -> Void)
typealias VoidClosure = (() -> Void)
typealias ResultHandler<Value> = (Result<Value>) -> Void
