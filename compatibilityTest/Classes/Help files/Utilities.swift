//
//  Utilities.swift
//  compatibilityTest
//
//  Created by spbiphones on 26.01.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation

func generateCode() -> String {
	
	let str = "qwertyuiopasdfghjklzxcvbnm1234567890"
	let arr = Array(str)
	
	func random() -> String {
		return String(describing: arr[Int.random(in: arr.indices)])
	}
	
	var result = ""
	let countOfCharacters = 12
	for _ in 0..<countOfCharacters {
		result += random()
	}
	return result
	
}
