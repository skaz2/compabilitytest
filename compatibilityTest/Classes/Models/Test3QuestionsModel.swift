//
//  Test1.swift
//  compatibilityTest
//
//  Created by spbiphones on 24.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

struct Test3QuestionsModel {
	var question1: String
	var question2: String
	var question3: String
	var question4: String
	var question5: String
	var question6: String
	//	let myDictionary = ["Hello": [1, 2, 3], "World": [4, 5, 6]]
	
	static func fetchTest3() -> [Test3QuestionsModel] {
		let item1 = Test3QuestionsModel(question1: "Да, смогу", question2: "Буду очень стараться", question3: "С моей ситуацией это невозможно", question4: "Меня это не интересует", question5: "Я уже живу в достатке и надеюсь через 5 лет будет только лучше", question6: "Другой ответ")
		
		let item2 = Test3QuestionsModel(question1: "Да, читаю художественные книги", question2: "Да читаю в основном про личную эффективность и психологию", question3: "Читаю в основном различные новости", question4: "Читаю развлекательный контент в интернете", question5: "Не люблю читать", question6: "Другой ответ")
		
		let item3 = Test3QuestionsModel(question1: "Да, я старался и был отличником", question2: "Сначала да, потом стало не интересно", question3: "Когда как", question4: "Считаю, что наш уровень образования и его подача отбивает иногда желание учиться у детей", question5: "Нет", question6: "Другой ответ")
		
		let item4 = Test3QuestionsModel(question1: "В принципе, все", question2: "Точные науки", question3: "Гуманитарные науки", question4: "Физкультура, труд, рисование", question5: "У меня часто менялись вкусы на любимые предметы", question6: "Другой ответ")
		
		let item5 = Test3QuestionsModel(question1: "Да, но развиваться ещё есть куда", question2: "Достаточно умён и профессионален в своей сфере работы, остальное не важно", question3: "Нет, все умные зануды", question4: "Не знаю", question5: "Другой ответ", question6: "")
		
		let item6 = Test3QuestionsModel(question1: "Я всё-таки умнее", question2: "Партнёр умнее меня", question3: "В чём-то я умнее в чём-то партнёр, так и дополняем друг друга", question4: "Другой ответ", question5: "", question6: "")
		
		let item7 = Test3QuestionsModel(question1: "Конечно", question2: "Если он действительно учился и закончил учёбу без посторонней помощи, то да", question3: "Не факт, человек может быть начитанным и знать свою профессию на отлично, но быть глупым по жизни", question4: "Другой ответ", question5: "", question6: "")
		
		let item8 = Test3QuestionsModel(question1: "Отлично", question2: "Нормально", question3: "Смотря чем, они в итоге занимаются", question4: "Мне с ними не интересно", question5: "Другой ответ", question6: "")
		
		let item9 = Test3QuestionsModel(question1: "Да, работаю почти без выходных", question2: "Нет, всё в меру ", question3: "По настроению", question4: "У меня не нормированный график. Иногда много, иногда нет.", question5: "Я не работаю", question6: "Другой ответ")
		
		let item10 = Test3QuestionsModel(question1: "Да очень", question2: "Да, но всё-таки думаю сменить вид деятельности", question3: "Уже привык, главное стабильность", question4: "Не доволен, ищу другую", question5: "Ненавижу свою работу", question6: "Другой ответ")
		
		let item11 = Test3QuestionsModel(question1: "Да моя работа — это моё призвание", question2: "Нет, я считаю я предназначен для другого", question3: "Я стремлюсь к этому", question4: "Я считаю, что у меня нет талантов, работаю, где могу", question5: "Я не работаю", question6: "Другой ответ")
		
		let item12 = Test3QuestionsModel(question1: "Да, вполне", question2: "Нет", question3: "Главное, что бы ему нравилось", question4: "Говорю ему что бы сменил", question5: "Он не работает", question6: "Другой ответ")
		
		let item13 = Test3QuestionsModel(question1: "Конечно, мы бы были отличной командой", question2: "Нельзя смешивать отношения и работу", question3: "Нет, я не люблю брать на себя много обязанностей", question4: "Я не уверен в партнёре и в его ответственности", question5: "Была бы возможность, я один бы этим занялся, без партнёра", question6: "Другой ответ")
		
		let item14 = Test3QuestionsModel(question1: "Абсолютно разные от появления человечества до новых технологий, я всегда люблю узнавать новое и это обсуждать", question2: "В основном о работе и семье", question3: "Предпочитаю молчать", question4: "Говорю обычно о своих планах, увлечениях", question5: "В основном люблю сначала слушать что бы понимать, о чем можно поговорить с конкретными людьми", question6: "Другой ответ")
		
		let item15 = Test3QuestionsModel(question1: "Уважаю и люблю", question2: "Я очень критично к себе отношусь", question3: "Хорошо", question4: "Нейтрально", question5: "Как и ко всем остальным", question6: "Другой ответ")
		
		let item16 = Test3QuestionsModel(question1: "Много, больше пяти", question2: "Один друг и этого достаточно", question3: "У меня нет друзей, только знакомые", question4: "Мои друзья – только близкие родственники", question5: "Нормально, от двух до четырёх", question6: "Другой ответ")
		
		let item17 = Test3QuestionsModel(question1: "Только с хорошо знакомыми людьми веду себя естественно и весело", question2: "Обычно мало говорю, больше слушаю", question3: "Привлекаете к себе внимание и много говорите на разные темы в любой компании", question4: "По настроению", question5: "Другой ответ", question6: "")
		
		let item18 = Test3QuestionsModel(question1: "Отвечу тем же", question2: "Не обращу внимания на оскорбление", question3: "Переведу в шутку, но тем самым тоже постараюсь задеть оппонента", question4: "Запомню и отомщу позже если будет возможность", question5: "Дам леща (ударю)", question6: "Другой ответ")
		
		let item19 = Test3QuestionsModel(question1: "У меня отличное здоровье, не болею", question2: "Плохое, часто болею", question3: "Болею в пределах нормы", question4: "Очень редко что-то беспокоит", question5: "Другой ответ", question6: "")
		
		let item20 = Test3QuestionsModel(question1: "Сколько получится", question2: "Хотел бы хотя бы до пенсии дожить", question3: "Максимально долго", question4: "40-50 лет, больше – неинтересно", question5: "Больше 100 лет", question6: "Другой ответ")
		
		let item21 = Test3QuestionsModel(question1: "Занимаюсь для поддержания тонуса и здоровья", question2: "Нет, мне не нравится", question3: "Нет, нету времени", question4: "Сейчас пока нет, но планирую", question5: "Занимаюсь профессионально", question6: "Другой ответ")
		
		let item22 = Test3QuestionsModel(question1: "Да, ем много, ем всё, не слежу за питанием", question2: "Люблю только вкусную еду", question3: "Я вегетарианец ", question4: "Ем мало – слежу за фигурой", question5: "Не питаю особой любви к еде", question6: "Другой ответ")
		
		let item23 = Test3QuestionsModel(question1: "Мне нравятся люди с формами, но не через чур", question2: "Как и к другим людям, внешность для меня не важна", question3: "Я считаю это отклонением в пищевом поведении или есть проблема, в здоровье которую нужно решать", question4: "Считаю их просто ленивыми", question5: "Другой ответ", question6: "")
		
		let item24 = Test3QuestionsModel(question1: "Да, остались последствия", question2: "Нет", question3: "Было средней тяжести без особых последствий", question4: "Не помню", question5: "Были только легкие заболевания/операции", question6: "Другой ответ")
		
		let item25 = Test3QuestionsModel(question1: "Да, но редко", question2: "Да, всегда", question3: "Нет, если партнёры спят отдельно значит у них что-то не так", question4: "Другой ответ", question5: "", question6: "")
		
		let item26 = Test3QuestionsModel(question1: "Есть, почти все, которые существуют, без них скучно", question2: "Есть но пытаюсь избавиться", question3: "Нет", question4: "Скорее это не привычки у меня, а периодическое веселье", question5: "Есть одна или две вредных привычки ", question6: "Другой ответ")
		
		let item27 = Test3QuestionsModel(question1: "Считаю это искусством, если сделано профессионально, но себе не хочу", question2: "Отлично у меня есть и много", question3: "Считаю, что люди себе этим просто портят тело", question4: "Хорошо, если в меру", question5: "Хочу себе сделать что – ни будь из этого", question6: "Другой ответ")
		
		let item28 = Test3QuestionsModel(question1: "Быть очень востребованным профессионалом своего дела и заработать", question2: "Иметь и развивать успешный бизнес", question3: "Украсть", question4: "Всё украдено до нас, их негде взять", question5: "Думаю, многим людям, это просто не реально", question6: "Другой ответ")
		
		let item29 = Test3QuestionsModel(question1: "Часто и расстраиваюсь иногда что у меня их не много", question2: "Не часто, они для меня не в приоритете", question3: "Думаю, не о деньгах скорее, а о способах заработка", question4: "Другой ответ", question5: "", question6: "")
		
		let item30 = Test3QuestionsModel(question1: "На развлечения", question2: "На себя и свои нужды", question3: "Поделился с родственниками/друзьями", question4: "Другой ответ", question5: "", question6: "")
		
		let item31 = Test3QuestionsModel(question1: "Не беру вообще, это допускаю только в экстренном и безвыходном случае", question2: "Бывает редко", question3: "Беру часто так как не хватает денег", question4: "Долгов много", question5: "Другой ответ", question6: "")
		
		let item32 = Test3QuestionsModel(question1: "Да и жил бы в свое удовольствие", question2: "Нет, мне нравится, чем я занимаюсь", question3: "Да, но занялся бы тем делом, которое было бы по душе", question4: "Мне не откуда сейчас увольняться и нет дел", question5: "Другой ответ", question6: "")
		
		let item33 = Test3QuestionsModel(question1: "Надо, но только если совсем они не могут прожить на свои", question2: "Не обязательно", question3: "Можно если есть лишние", question4: "Надо в любом случае", question5: "Другой ответ", question6: "")
		
		let item34 = Test3QuestionsModel(question1: "Буду всеми силами искать источник дохода", question2: "Возьму сначала в долг потом отдам", question3: "Возьму у близких родственников безвозмездно", question4: "Ничего, потерплю", question5: "Другой ответ", question6: "")
		
		let item35 = Test3QuestionsModel(question1: "Не обязательно каждый месяц — это делать", question2: "Раз в месяц точно", question3: "Я это делаю только по острой необходимости", question4: "Пару раз в месяц", question5: "Я «шопоголик» чем чаще, тем лучше", question6: "Другой ответ")
		
		let item36 = Test3QuestionsModel(question1: "Да, мне важнее позаботится о любимом человеке", question2: "Нет, не могу экономить на себе в пользу других", question3: "Могу, но только в необходимых случаях", question4: "Другой ответ", question5: "", question6: "")
		
		let item37 = Test3QuestionsModel(question1: "Да, должен", question2: "Только если он захочет", question3: "Необязательно", question4: "Не должен, лучше, когда каждый за себя платит", question5: "Другой ответ", question6: "")
		
		let item38 = Test3QuestionsModel(question1: "Да, никогда ничего не предугадаешь", question2: "Нужно выбирать правильного партнёра что бы быть в нём уверенным и такое не допускать", question3: "Даже не думал бы об этом", question4: "Другой ответ", question5: "", question6: "")
		
		let item39 = Test3QuestionsModel(question1: "Да, должны", question2: "Только если они в браке и это семья с детьми, а так нет", question3: "Если один из партнёров действительно не может заплатить и у него проблемы, то можно помочь", question4: "Нет если я к этому не имею отношения, и партнёр все это брал без моего участия", question5: "Другой ответ", question6: "")
		
		let item40 = Test3QuestionsModel(question1: "Должен быть общий бюджет в одном месте и по согласию сторон его тратить на семью", question2: "Помимо общего бюджета должны быть личные деньги для ухода за собой или на свои хобби", question3: "У каждого свой бюджет, так удобнее", question4: "Другой ответ", question5: "", question6: "")

		
		
		return [item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, item14, item15, item16, item17, item18, item19, item20, item21, item22, item23, item24, item25, item26, item27, item28, item29, item30, item31, item32, item33, item34, item35, item36, item37, item38, item39, item40]
	}
	
}
