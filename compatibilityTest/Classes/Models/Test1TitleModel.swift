//
//  Test1.swift
//  compatibilityTest
//
//  Created by spbiphones on 24.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

struct Test1TitleModel {
	var testLabel: String
	
//	let myDictionary = ["Hello": [1, 2, 3], "World": [4, 5, 6]]
	
	static func fetchTest1() -> [Test1TitleModel] {
		let item1 = Test1TitleModel(testLabel: "Вам нравится внешность вашего избранника?")
		let item2 = Test1TitleModel(testLabel: "Вас полностью устраивает возраст вашего избранника?")
		let item3 = Test1TitleModel(testLabel: "Вы любите объятия и поцелуи от вашего партнёра?")
		let item4 = Test1TitleModel(testLabel: "Какое у вас возникает чувство, когда вы видите целующуюся пару в общественном месте?")
		let item5 = Test1TitleModel(testLabel: "Нужно ли признаваться в любви и как часто своему партнёру?")
		let item6 = Test1TitleModel(testLabel: "Оцените готовность прямо сейчас признаться в любви своему партнёру?")
		let item7 = Test1TitleModel(testLabel: "Можно ли со временем полюбить человека, который сейчас любит, а вы его – нет?")
		let item8 = Test1TitleModel(testLabel: "Вам важно знать всё о прошлой жизни вашего партнёра?")
		let item9 = Test1TitleModel(testLabel: "Как вы считаете, можно ли спрашивать о количестве сексуальных связей партнёра до встречи с вами?")
		let item10 = Test1TitleModel(testLabel: "Вы хотели бы детей от своего партнёра?")
		let item11 = Test1TitleModel(testLabel: "Вы верите в любовь с первого взгляда?")
		let item12 = Test1TitleModel(testLabel: "Что вы будете делать, если вы – любите, а вас нет?")
		let item13 = Test1TitleModel(testLabel: "Вы бы смогли простить измену вашему партнёру?")
		let item14 = Test1TitleModel(testLabel: "Можно ли изменить своему партнёру за большие деньги, если кто-то из близких смертельно болен?")
		let item15 = Test1TitleModel(testLabel: "Как вы охарактеризуете свой сексуальный темперамент?")
		let item16 = Test1TitleModel(testLabel: "Вас всё устраивает в сексе с вашим нынешним партнёром?")
		let item17 = Test1TitleModel(testLabel: "Можно ли расстаться с партнёром от недостатка в сексе?")
		let item18 = Test1TitleModel(testLabel: "Был ли у вас в жизни секс без чувств к человеку?")
		let item19 = Test1TitleModel(testLabel: "Готовы ли вы пойти на любые эксперименты в сексе ради вашего партнера?")
		let item20 = Test1TitleModel(testLabel: "Как часто вы хотели бы заниматься сексом с вашим партнёром?")
		let item21 = Test1TitleModel(testLabel: "Как вы думаете, в прошлом вы любили по-настоящему?")
		let item22 = Test1TitleModel(testLabel: "Вы ранее состояли в официальном браке?")
		let item23 = Test1TitleModel(testLabel: "Как вы относитесь к гражданскому браку?")
		let item24 = Test1TitleModel(testLabel: "Сколько времени, по-вашему, нужно быть вместе паре до брака?")
		let item25 = Test1TitleModel(testLabel: "Часто ли вы прибегаете к манипуляции в отношениях?")
		let item26 = Test1TitleModel(testLabel: "Если бы ваш партнёр вам изменил как вы думаете в чем причина?")
		let item27 = Test1TitleModel(testLabel: "Можно ли прожить всю жизнь с одним человеком без измен?")
		let item28 = Test1TitleModel(testLabel: "Во сколько лет, по вашему мнению, можно вступать в брак и создавать семью?")
		let item29 = Test1TitleModel(testLabel: "Как вы относитесь к многодетным семьям (более трёх детей)? ")
		let item30 = Test1TitleModel(testLabel: "Сколько вы хотите иметь детей?")
		let item31 = Test1TitleModel(testLabel: "Как вы относитесь к абортам?")
		let item32 = Test1TitleModel(testLabel: "Хотели бы вы при родах общего ребёнка находится в одном помещении с вашим партнёром?")
		let item33 = Test1TitleModel(testLabel: "Вы бы смогли воспитывать чужого ребёнка?")
		let item34 = Test1TitleModel(testLabel: "Вы бы смогли отказаться от своего ребёнка инвалида?")
		let item35 = Test1TitleModel(testLabel: "Вы понимаете, что в семейной жизни присутствует много бытовых разногласий, которые могут разладить ваши отношения?")
		let item36 = Test1TitleModel(testLabel: "Можно ли разойтись с партнёром из-за бытовых проблем?")
		let item37 = Test1TitleModel(testLabel: "Какие взаимоотношения для вас ближе из перечисленных?")
		let item38 = Test1TitleModel(testLabel: "Счастливое ли у вас было детство?")
		let item39 = Test1TitleModel(testLabel: "Вы любите своих родителей? Часто говорите об этом им?")
		let item40 = Test1TitleModel(testLabel: "Выберите, что вам удобнее для совместного проживания?")
		let item41 = Test1TitleModel(testLabel: "Как вы относитесь к разводам?")
		let item42 = Test1TitleModel(testLabel: "Как, по-вашему, более правильно воспитывать детей?")
		let item43 = Test1TitleModel(testLabel: "Кто должен больше уделять времени в воспитании детей?")
		let item44 = Test1TitleModel(testLabel: "Если бы вы могли дать только один совет своему ребёнку, какой бы вы дали?")
		let item45 = Test1TitleModel(testLabel: "Когда вы впервые увидели своего нынешнего партнёра что вы подумали?")
		let item46 = Test1TitleModel(testLabel: "Ваши самые длительные отношения в жизни?")
		let item47 = Test1TitleModel(testLabel: "Сколько серьёзных отношений за всю жизнь у вас было?")
		let item48 = Test1TitleModel(testLabel: "Сколько мимолётных несерьёзных влечений с продолжением на секс у вас было?")
		let item49 = Test1TitleModel(testLabel: "Как вы думаете, может ли развестись пара, если женщина не выполняет свои обязанности по организации быта и домашнего уюта?")
		let item50 = Test1TitleModel(testLabel: "Как вы думаете, может ли развестись пара, если муж не выполняет свои обязанности по обеспечению семьи?")
		let item51 = Test1TitleModel(testLabel: "Если один из партнёров употребляет алкоголь или наркотики и никакие уговоры не действуют нужно ли разойтись?")
		let item52 = Test1TitleModel(testLabel: "У вас есть судимые в семье?")
		let item53 = Test1TitleModel(testLabel: "Хотели бы вы через год видеть вашего нынешнего партнёра рядом с вами?")
		let item54 = Test1TitleModel(testLabel: "Самые важные для вас недостатки в вашем нынешнем партнёре которые хотелось бы изменить, сколько их?")
		let item55 = Test1TitleModel(testLabel: "Что вам больше всего нравится в вашем партнёре?")
		let item56 = Test1TitleModel(testLabel: "Вы изменяли хоть раз в своей жизни будучи в серьёзных отношениях или находясь в браке?")
		let item57 = Test1TitleModel(testLabel: "Когда вы поняли, что действительно полюбили вашего нынешнего партнёра?")
		let item58 = Test1TitleModel(testLabel: "Какая модель общения у вас с вашим партнёром во время спора или размолвки?")
		let item59 = Test1TitleModel(testLabel: "Где и как вы мечтаете жить с семьёй и детьми?")
		let item60 = Test1TitleModel(testLabel: "Куда прямо сейчас вы бы отправились в путешествие со своим партнёром?")
		
//		return [item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, item14, item15, item16, item17, item18, item19, item20, item21, item22, item23, item24, item25, item26, item27, item28, item29, item30, item31, item32, item33, item34, item35, item36,
//		item37, item38, item39, item40, item41, item42, item43, item44, item45, item46, item47, item48, item49, item50, item51, item52, item53, item54, item55, item56, item57, item58, item59, item60]
		return [item1, item2]
	}
	
}
