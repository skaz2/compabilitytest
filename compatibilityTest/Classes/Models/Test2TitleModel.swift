//
//  Test1.swift
//  compatibilityTest
//
//  Created by spbiphones on 24.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

struct Test2TitleModel {
	var testLabel: String
	
//	let myDictionary = ["Hello": [1, 2, 3], "World": [4, 5, 6]]
	
	static func fetchTest2() -> [Test2TitleModel] {
		let item1 = Test2TitleModel(testLabel: "Бываете ли вы ленивым человеком?")
		let item2 = Test2TitleModel(testLabel: "Ваши 2 основные черты характера по отношению к другим людям?")
		let item3 = Test2TitleModel(testLabel: "Ваши 2 основные черты характера по отношению к делу?")
		let item4 = Test2TitleModel(testLabel: "Ваши 2 основные черты характера по отношению к себе?")
		let item5 = Test2TitleModel(testLabel: "Ваши 2 основные черты характера к собственности?")
		let item6 = Test2TitleModel(testLabel: "Сколько часов в сутки в целом вы спите?")
		let item7 = Test2TitleModel(testLabel: "Вы ревнивый человек?")
		let item8 = Test2TitleModel(testLabel: "Чем вас можно действительно разозлить?")
		let item9 = Test2TitleModel(testLabel: "Как часто вы злитесь?")
		let item10 = Test2TitleModel(testLabel: "Вам часто приходилось драться за себя и других людей?")
		let item11 = Test2TitleModel(testLabel: "Обижали ли вас в детстве ровесники?")
		let item12 = Test2TitleModel(testLabel: "Обижали ли вас родители в детстве?")
		let item13 = Test2TitleModel(testLabel: "Как часто вы используете нецензурную лексику?")
		let item14 = Test2TitleModel(testLabel: "Внимательно ли вы слушаете людей при разговоре?")
		let item15 = Test2TitleModel(testLabel: "Какой вид юмора вы предпочитаете?")
		let item16 = Test2TitleModel(testLabel: "Вы считаете себя добрым человеком?")
		let item17 = Test2TitleModel(testLabel: "Какие качества из перечисленных в людях вы считаете самыми плохими?")
		let item18 = Test2TitleModel(testLabel: "Сентиментальный ли вы человек?")
		let item19 = Test2TitleModel(testLabel: "В каких случаях вы можете обидеться на близких вам людей?")
		let item20 = Test2TitleModel(testLabel: "Вы бы могли переехать жить в другую страну, если вас об этом попросит партнёр?")
		let item21 = Test2TitleModel(testLabel: "Что на данный момент для вас важнее в жизни?")
		let item22 = Test2TitleModel(testLabel: "Злитесь ли вы если оказывается, что ваш партнёр прав, а вы нет?")
		let item23 = Test2TitleModel(testLabel: "Вас раздражает, когда ваш партнёр долго говорит по телефону или переписывается?")
		let item24 = Test2TitleModel(testLabel: "Какой досуг из цифровых развлечений вам близок?")
		let item25 = Test2TitleModel(testLabel: "К чему вы больше всего проявляете интерес из перечисленных вариантов?")
		let item26 = Test2TitleModel(testLabel: "Вы смогли бы прожить год на необитаемом острове?")
		let item27 = Test2TitleModel(testLabel: "Как вы относитесь к домашним животным?")
		let item28 = Test2TitleModel(testLabel: "Как вы относитесь к религиозным людям?")
		let item29 = Test2TitleModel(testLabel: "Какую музыку вы предпочитаете слушать?")
		let item30 = Test2TitleModel(testLabel: "Если ваш партнёр любит музыку, которая вам не нравится, вы будете его убеждать разными способами ее не слушать?")
		let item31 = Test2TitleModel(testLabel: "Считаете ли вы себя счастливым человеком?")
		let item32 = Test2TitleModel(testLabel: "Как вы считаете кто более развит в плане построений правильных отношений в вашей паре?")
		let item33 = Test2TitleModel(testLabel: "Как часто ваши мнения с партнёром расходятся?")
		let item34 = Test2TitleModel(testLabel: "Вы любите спорить?")
		let item35 = Test2TitleModel(testLabel: "Считаете ли вы важным развиваться духовно?")
		let item36 = Test2TitleModel(testLabel: "Вы медитируете?")
		let item37 = Test2TitleModel(testLabel: "Вы мастурбируете?")
		let item38 = Test2TitleModel(testLabel: "Как вы считаете позволительна ли мастурбация в отношениях?")
		let item39 = Test2TitleModel(testLabel: "Как вы считаете, что вас отличает от большинства людей?")
		let item40 = Test2TitleModel(testLabel: "Как вы считаете есть ли у вас комплексы?")
		let item41 = Test2TitleModel(testLabel: "Вы верите в магию/гадание/гороскопы?")
		let item42 = Test2TitleModel(testLabel: "Вас может раздражать в партнёре бытовая неаккуратность либо неряшливость?")
		let item43 = Test2TitleModel(testLabel: "Если бы вы были единственным подходящим донором для своего партнёра, отдали бы вы свою почку если от этого зависела его жизнь?")
		let item44 = Test2TitleModel(testLabel: "Много времени вы проводите в интернете и с какой целью?")
		let item45 = Test2TitleModel(testLabel: "У вас много друзей?")
		
		
		return [item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, item14, item15, item16, item17, item18, item19, item20, item21, item22, item23, item24, item25, item26, item27, item28, item29, item30, item31, item32, item33, item34, item35, item36,
		item37, item38, item39, item40, item41, item42, item43, item44, item45]
	}
	
}
