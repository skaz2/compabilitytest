//
//  Test1.swift
//  compatibilityTest
//
//  Created by spbiphones on 24.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

struct Test1QuestionsModel {
	var question1: String
	var question2: String
	var question3: String
	var question4: String
	var question5: String
	var question6: String
	//	let myDictionary = ["Hello": [1, 2, 3], "World": [4, 5, 6]]
	
	static func fetchTest1() -> [Test1QuestionsModel] {
		let item1 = Test1QuestionsModel(question1: "Да, очень", question2: "Нравится", question3: "Нравится со всеми недостатками", question4: "Главное в нём – не внешняя, а внутренняя красота", question5: "Хотел бы немного изменить внешность партнёра", question6: "Другой ответ")
		
		let item2 = Test1QuestionsModel(question1: "Да", question2: "Хотелось бы немного старше", question3: "Хотелось бы немного моложе", question4: "Возраст не имеет значения", question5: "Уже полностью смирился с возрастом своего партнёра", question6: "Другой ответ")
		
		let item3 = Test1QuestionsModel(question1: "Да, очень", question2: "Не люблю", question3: "Обычно обнимаю и целую я, а не меня", question4: "Иногда раздражают", question5: "Другой ответ ", question6: "")
		
		let item4 = Test1QuestionsModel(question1: "Смущение", question2: "Стыд за них", question3: "Зависть", question4: "Безразличие", question5: "Радость за них", question6: "Другой ответ")
		
		let item5 = Test1QuestionsModel(question1: "Каждый день", question2: "Пару раз в неделю", question3: "Часто не нужно", question4: "Одного раза хватит", question5: "Я поступками выражаю свою любовь", question6: "Другой ответ")
		
		let item6 = Test1QuestionsModel(question1: "10-30", question2: "30-50", question3: "50-50", question4: "50-70", question5: "70-90", question6: "90-100")
		
		let item7 = Test1QuestionsModel(question1: "Нужно что бы сразу оба полюбили", question2: "Всё может быть", question3: "Можно, но вряд ли", question4: "Если сразу не пришли чувства, то лучше и не надеяться", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item8 = Test1QuestionsModel(question1: "Да, важно: по прошлому можно лучше понять человека", question2: "Прошлое – не важно, главное – то, что происходит сейчас", question3: "Очень. Но я стесняюсь задавать некоторые вопросы об этом", question4: "Боюсь разочароваться", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item9 = Test1QuestionsModel(question1: "Можно спросить если уверен, что не расстроишься", question2: "Думаю, нет", question3: "Нужно", question4: "Мне это не важно", question5: "Боюсь встречного ответа", question6: "Другой ответ")
		
		let item10 = Test1QuestionsModel(question1: "Да", question2: "Нет", question3: "Возможно", question4: "Не очень люблю детей", question5: "Уже есть общие дети", question6: "Другой ответ")
		
		let item11 = Test1QuestionsModel(question1: "О да, у нас прям так и было", question2: "Не верю, это глупо", question3: "Верю, так может быть", question4: "Любовь возникает только со временем", question5: "У меня было такое", question6: "Другой ответ")
		
		let item12 = Test1QuestionsModel(question1: "Добиваться взаимной любви", question2: "Разорву все связи с этим человеком", question3: "Буду надеяться на взаимность", question4: "Страдать", question5: "Искать другой объект для внимания к себе", question6: "Другой ответ")
		
		let item13 = Test1QuestionsModel(question1: "Да", question2: "Нет", question3: "Смотря, какая измена", question4: "Сначала отомщу, а там посмотрим", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item14 = Test1QuestionsModel(question1: "Да", question2: "Нет", question3: "Можно, если никто не узнает", question4: "Я попробую одолжить или заработать по-другому", question5: "Конечно, пойду на всё ради спасения жизни", question6: "Другой ответ")
		
		let item15 = Test1QuestionsModel(question1: "Ласковый и нежный", question2: "Страстный и жесткий", question3: "По настроению", question4: "Смотря, какой партнёр", question5: "Пассивный люблю, когда всё делают за меня", question6: "Другой ответ")
		
		let item16 = Test1QuestionsModel(question1: "Абсолютно, я получаю удовольствие всегда", question2: "Хочу больше страсти", question3: "Хочу больше внимания к моим желаниям", question4: "Меня не устраивает сейчас наш секс", question5: "Не всё, я стараюсь, а он нет", question6: "У нас нет секса")
		
		let item17 = Test1QuestionsModel(question1: "Конечно", question2: "Возможно", question3: "Смотря какие обстоятельства на это повлияли", question4: "Это не главное", question5: "Без секса нет любви", question6: "Другой ответ")
		
		let item18 = Test1QuestionsModel(question1: "Да", question2: "Нет", question3: "Я только по любви", question4: "Ради опыта был", question5: "Был в не трезвом состоянии в какой-то компании", question6: "Всякое было")
		
		let item19 = Test1QuestionsModel(question1: "Конечно", question2: "Я не приемлю этого", question3: "Если это только без посторонних людей и боли", question4: "Мы любим разные эксперименты", question5: "Я стесняюсь", question6: "Другой ответ")
		
		let item20 = Test1QuestionsModel(question1: "Каждый день", question2: "Через день", question3: "Пару раз в неделю", question4: "Раз в месяц", question5: "Мне это не важно", question6: "Другой ответ")
		
		let item21 = Test1QuestionsModel(question1: "Да", question2: "Нет", question3: "Несколько раз", question4: "Казалось, что да, но на самом деле нет", question5: "Не знаю", question6: "Другой ответ")
		
		let item22 = Test1QuestionsModel(question1: "Да", question2: "Нет, но хочу", question3: "Два раза", question4: "Более двух", question5: "Нет и не собираюсь пока", question6: "Другой ответ")
		
		let item23 = Test1QuestionsModel(question1: "Отлично", question2: "Нормально", question3: "Не очень", question4: "Плохо", question5: "Я считаю это не серьезно", question6: "Другой ответ")
		
		let item24 = Test1QuestionsModel(question1: "Не менее года", question2: "Пока не появятся дети", question3: "Брак не обязателен", question4: "Минимум полгода", question5: "Как получится ", question6: "Другой ответ")
		
		let item25 = Test1QuestionsModel(question1: "Бывает", question2: "Да, часто", question3: "Стараюсь так не делать", question4: "Только в пользу наших совместных отношений", question5: "Нет", question6: "Другой ответ")
		
		let item26 = Test1QuestionsModel(question1: "Недостаток внимания и любви от вас", question2: "Партнёр значит легкомысленный", question3: "Прошла любовь завяли кактусы", question4: "Ему не хватало поддержки от вас", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item27 = Test1QuestionsModel(question1: "Крайне сложно", question2: "Можно, но сомневаюсь, что мне не изменят", question3: "Конечно, если партнёры дают друг другу всё", question4: "Нет", question5: "Другой ответ", question6: "")
		
		let item28 = Test1QuestionsModel(question1: "18-22", question2: "22-25", question3: "25-30", question4: "Только после 30", question5: "От 18 лет, по взаимной любви, с умственной зрелостью и ответственностью перед партнёром и будущими детьми", question6: "")
		
		let item29 = Test1QuestionsModel(question1: "Как к счастливой семье", question2: "Не понимаю зачем столько детей", question3: "Как к легкомысленным людям, особенно бедным", question4: "Если семья живёт в достатке, то хорошо", question5: "Жаль детей, кто-то из них точно не получит нужной родительской любви и внимания", question6: "")
		
		let item30 = Test1QuestionsModel(question1: "Одного ребёнка", question2: "Двоих", question3: "Чем больше, тем лучше", question4: "Количество не важно", question5: "Не хочу детей пока и не думаю об этом", question6: "Другой ответ")
		
		let item31 = Test1QuestionsModel(question1: "Категорично против", question2: "Нормально, всякое бывает", question3: "Плохо, но допускаю при крайней необходимости", question4: "Мне всё равно", question5: "Другой ответ", question6: "")
		
		let item32 = Test1QuestionsModel(question1: "Да, мне бы было приятно", question2: "Нет, мне бы было не приятно", question3: "Поддержка при взаимном согласии очень важна в такой ситуации", question4: "Это слишком личное и отношения в сексуальной жизни могут испортиться ", question5: "Другой ответ", question6: "")
		
		let item33 = Test1QuestionsModel(question1: "Да, если он не проблемный", question2: "Нет, даже если он и хороший", question3: "Смотря какой возраст", question4: "Нет чужих детей", question5: "Смотря, какой партнёр и его ребёнок в целом", question6: "Другой ответ")
		
		let item34 = Test1QuestionsModel(question1: "Да, не хочу мучаться", question2: "Нет, пойду на всё ради него", question3: "Если инвалидность тяжелая, то да", question4: "Отдам родителям", question5: "Другой ответ", question6: "")
		
		let item35 = Test1QuestionsModel(question1: "Прекрасно понимаю", question2: "Понимаю и знаю как их решить", question3: "Не считаю это проблемой, если пара умеет договариваться", question4: "Нужно заранее решить кто в чём принимает решения", question5: "Нужно выбрать главного во всём и с него спрос", question6: "Другой ответ")
		
		let item36 = Test1QuestionsModel(question1: "Конечно, хочу жить в комфорте", question2: "Нет, любовь дороже, возьму всё на себя", question3: "Буду до последнего пробовать договориться", question4: "Буду ссориться пока не поступят по-моему", question5: "Нужно помогать друг другу во всём, возможно даже учится чему-то и проблем не будет", question6: "Другой ответ")
		
		let item37 = Test1QuestionsModel(question1: "Спокойные и стабильные", question2: "Как на вулкане, с нервами, переживаниями и любовной страстью", question3: "Нежность, забота о друг друге и поддержка", question4: "Веселье и отдых от внешнего мира", question5: "Без обязательств", question6: "Другой ответ")
		
		let item38 = Test1QuestionsModel(question1: "Обычное, как у большинства", question2: "Да, но есть детская травма", question3: "Очень счастливое, мне дали всё что могли", question4: "Из-за негативного воздействия близких людей был не счастлив", question5: "Я был счастлив только когда оставался один ", question6: "Другой ответ")
		
		let item39 = Test1QuestionsModel(question1: "Очень и говорю об этом часто", question2: "У меня осталась обида с детства, поэтому не говорю", question3: "Говорю очень редко, стесняюсь", question4: "Люблю и стараюсь показывать делами, слова не так важны", question5: "Да, но только недавно начал говорить об этом", question6: "Другой ответ")
		
		let item40 = Test1QuestionsModel(question1: "Своя семья без детей и близких родственников", question2: "Семья и близкие родственники", question3: "Своя семья с детьми", question4: "Мне пока нравится жить с родственниками", question5: "Мне пока нравится жить одному", question6: "Другой ответ")
		
		let item41 = Test1QuestionsModel(question1: "Плохо. Это как разрушение семьи", question2: "Отлично, было бы хуже, если бы законом это было запрещено", question3: "Зачем тогда жениться", question4: "Развод часть жизни если ошибся с выбором", question5: "Другой ответ", question6: "")
		
		let item42 = Test1QuestionsModel(question1: "Подавать пример", question2: "В строгости", question3: "В любви и заботе", question4: "В любви и в строгости что бы уважали родителей", question5: "Быть другом во всём и учить чувству справедливости", question6: "Другой ответ")
		
		let item43 = Test1QuestionsModel(question1: "Отец", question2: "Мать", question3: "Оба родителя постоянно", question4: "Родители и все близкие родственники", question5: "Если речь о девочке мама, если о мальчике отец", question6: "Другой ответ")
		
		let item44 = Test1QuestionsModel(question1: "Постоянно учись и повышай квалификацию", question2: "Делай и твори только хорошее, и добро к тебе вернется", question3: "Никогда не сдавайся", question4: "Не верь никому ", question5: "Будь справедлив", question6: "Другой ответ")
		
		let item45 = Test1QuestionsModel(question1: "Какой он симпатичный", question2: "Он мне поначалу не понравился", question3: "Мне нравилось наблюдать за ним", question4: "Мне нравилось говорить с ним", question5: "Чувствовалось смущение из-за сильной симпатии", question6: "Другой ответ")
		
		let item46 = Test1QuestionsModel(question1: "Меньше, чем полгода", question2: "Год", question3: "Два года", question4: "Больше трёх лет", question5: "Больше шести лет", question6: "Другой ответ")
		
		let item47 = Test1QuestionsModel(question1: "1-7", question2: "8-15", question3: "16-21", question4: "22-35", question5: "Более 35ти", question6: "Другой ответ")
		
		let item48 = Test1QuestionsModel(question1: "1-7", question2: "8-15", question3: "16-21", question4: "22-35", question5: "Более 35", question6: "Другой ответ")
		
		let item49 = Test1QuestionsModel(question1: "Конечно", question2: "Не разведутся, если всё остальное хорошо", question3: "Рано или поздно разведутся из-за ссор", question4: "Нет, если женщина будет стараться и учится этому", question5: "Другой ответ", question6: "")
		
		let item50 = Test1QuestionsModel(question1: "Конечно", question2: "Не разведутся, если всё остальное хорошо", question3: "Рано или поздно разведутся из-за ссор", question4: "Нет, если мужчина будет стараться и учится этому", question5: "Другой ответ", question6: "")
		
		let item51 = Test1QuestionsModel(question1: "Конечно", question2: "Попробую помочь в лечении, а там посмотрим", question3: "Нет, от любимых не отказываются", question4: "Смотря в каких количествах", question5: "Попробую тоже что бы понять, что он в этом нашел", question6: "Другой ответ")
		
		let item52 = Test1QuestionsModel(question1: "Есть – за незначительные нарушения закона", question2: "Нет", question3: "Есть – за серьезные нарушения", question4: "Не знаю", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item53 = Test1QuestionsModel(question1: "Да конечно", question2: "Если он не изменится в некоторых вещах, то нет", question3: "Пока не знаю", question4: "Хочу всегда быть вместе", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item54 = Test1QuestionsModel(question1: "1-2", question2: "Больше трёх", question3: "Значительных нет, он пытается быть лучше", question4: "Их многовато", question5: "Их нет", question6: "Другой ответ")
		
		let item55 = Test1QuestionsModel(question1: "Честность и доброта", question2: "Красота и харизма", question3: "Ум и чувство юмора", question4: "Влияние в обществе и финансовая независимость", question5: "Забота о вас", question6: "Другой ответ")
		
		let item56 = Test1QuestionsModel(question1: "Да, изменял только с разными людьми", question2: "Никогда", question3: "Было до 5 раз", question4: "Был постоянный партнёр для измен", question5: "Было несколько постоянных партнёров", question6: "Другой ответ")
		
		let item57 = Test1QuestionsModel(question1: "Почти сразу", question2: "Спустя несколько месяцев", question3: "Когда полностью доверились и открылись друг другу", question4: "Спустя очень долгое время", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item58 = Test1QuestionsModel(question1: "Обычно я всё решаю и со мной партнёр не спорит", question2: "Много обсуждаем и разговариваем пока не придём к общему и правильному решению", question3: "Я всегда стараюсь первым сгладить ситуацию", question4: "Обычно партнёр пытается сгладить ситуацию", question5: "Другой ответ", question6: "")
		
		let item59 = Test1QuestionsModel(question1: "В доме в пригороде со своим двором", question2: "В хорошей квартире в центре мегаполиса", question3: "Мне всё равно где", question4: "Где-нибудь на берегу моря", question5: "Пока не знаю", question6: "Другой ответ")
		
		let item60 = Test1QuestionsModel(question1: "Всё равно, главное тепло и пляж", question2: "Туда, где можно активно отдохнуть лыжи, сноуборд и т.д.", question3: "В какое-то очень необычное место без цивилизации", question4: "В дорогое место с высоким уровнем дохода и обслуживания", question5: "Не хочу сейчас никуда", question6: "Другой ответ")
		
		
		return [item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, item14, item15, item16, item17, item18, item19, item20, item21, item22, item23, item24, item25, item26, item27, item28, item29, item30, item31, item32, item33, item34, item35, item36, item37, item38, item39, item40, item41, item42, item43, item44, item45, item46, item47, item48, item49, item50, item51, item52, item53, item54, item55, item56, item57, item58, item59, item60]
	}
	
}
