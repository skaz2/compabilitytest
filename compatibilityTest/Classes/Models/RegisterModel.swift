//
//  Register.swift
//  compatibilityTest
//
//  Created by spbiphones on 26.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation

class RegisterModel {
	var code: String?
	var userId: String?
	
	var dict: [String: Any] {
		return [
			"code": code ?? ""
		]
	}
	
	init() {
		userId = ""
	}
}
