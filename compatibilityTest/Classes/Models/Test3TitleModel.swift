//
//  Test1.swift
//  compatibilityTest
//
//  Created by spbiphones on 24.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

struct Test3TitleModel {
	var testLabel: String
	
//	let myDictionary = ["Hello": [1, 2, 3], "World": [4, 5, 6]]
	
	static func fetchTest3() -> [Test3TitleModel] {
		let item1 = Test3TitleModel(testLabel: "Как вы думаете, сможете ли вы через 5 лет иметь всегда высокий и стабильный доход?")
		let item2 = Test3TitleModel(testLabel: "Любите ли вы читать?")
		let item3 = Test3TitleModel(testLabel: "Вам нравилось ходить в школу и получать новые знания?")
		let item4 = Test3TitleModel(testLabel: "Какие предметы вам нравились в школе больше всех?")
		let item5 = Test3TitleModel(testLabel: "Вы себя считаете умным человеком?")
		let item6 = Test3TitleModel(testLabel: "Как вы считаете кто более умён вы или ваш партнёр?")
		let item7 = Test3TitleModel(testLabel: "Как вы считаете высшее образование и красный диплом это признак ума и интеллекта человека?")
		let item8 = Test3TitleModel(testLabel: "Как вы относитесь к людям у которых нет полного образования либо учились в ПТУ?")
		let item9 = Test3TitleModel(testLabel: "Вы много времени уделяете своей работе?")
		let item10 = Test3TitleModel(testLabel: "Вы довольны своей работой?")
		let item11 = Test3TitleModel(testLabel: "Как вы считаете вы работаете по призванию?")
		let item12 = Test3TitleModel(testLabel: "Вы довольны работой своего партнёра?")
		let item13 = Test3TitleModel(testLabel: "Если была бы возможность, вы бы открыли общее дело с вашим партнёром?")
		let item14 = Test3TitleModel(testLabel: "Какие темы для разговора вы больше всего обсуждаете в социуме?")
		let item15 = Test3TitleModel(testLabel: "Как вы относитесь к себе?")
		let item16 = Test3TitleModel(testLabel: "Много ли у вас друзей?")
		let item17 = Test3TitleModel(testLabel: "Как вы обычно ведёте себя в компании?")
		let item18 = Test3TitleModel(testLabel: "Если вас грубо оскорбят не знакомые вам люди, что вы сделаете?")
		let item19 = Test3TitleModel(testLabel: "Как вы считаете, насколько вы здоровый человек?")
		let item20 = Test3TitleModel(testLabel: "Сколько лет вы бы хотели прожить?")
		let item21 = Test3TitleModel(testLabel: "Занимаетесь ли вы каким, либо спортом?")
		let item22 = Test3TitleModel(testLabel: "Вы любите поесть?")
		let item23 = Test3TitleModel(testLabel: "Как вы относитесь к полным людям?")
		let item24 = Test3TitleModel(testLabel: "Вы когда – ни будь переносили тяжелое заболевание опасное для жизни или сложные операции?")
		let item25 = Test3TitleModel(testLabel: "Как вы считаете должны ли партнёры спать отдельно что бы хорошо выспаться?")
		let item26 = Test3TitleModel(testLabel: "Есть ли у вас вредные привычки курение, алкоголь, наркотики?")
		let item27 = Test3TitleModel(testLabel: "Как вы относитесь к различным модификациям тела (тату, пирсинг и т.д.)?")
		let item28 = Test3TitleModel(testLabel: "Где бы вы могли достать большие деньги по вашему мнению?")
		let item29 = Test3TitleModel(testLabel: "Как часто и что вы думаете о деньгах?")
		let item30 = Test3TitleModel(testLabel: "На что вы потратили свои первые заработанные деньги?")
		let item31 = Test3TitleModel(testLabel: "Вы часто берёте в долг?")
		let item32 = Test3TitleModel(testLabel: "Вы бы уволились с работы или прекратили бы заниматься тем делом, которым занимаетесь сейчас, если у вас бы появилось много денег?")
		let item33 = Test3TitleModel(testLabel: "Надо ли помогать своим родителям деньгами?")
		let item34 = Test3TitleModel(testLabel: "Если у вас совсем не окажется денег, что вы будете делать?")
		let item35 = Test3TitleModel(testLabel: "Как вы думаете сколько раз в месяц нужно ходить на «шоппинг»?")
		let item36 = Test3TitleModel(testLabel: "Вы бы экономили на себе в пользу вашего партнёра?")
		let item37 = Test3TitleModel(testLabel: "Как вы считаете, должен ли мужчина платить в ресторане, кино и т.д. за свою женщину?")
		let item38 = Test3TitleModel(testLabel: "Если бы вы были очень богатым человеком подписывали бы брачный контракт со своим партнёром перед свадьбой?")
		let item39 = Test3TitleModel(testLabel: "Как вы считаете, должны ли партнёры выплачивать за друг друга ипотеку, кредиты, долги?")
		let item40 = Test3TitleModel(testLabel: "Как в семье должен вестись бюджет по вашему мнению?")

		
		
		return [item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, item14, item15, item16, item17, item18, item19, item20, item21, item22, item23, item24, item25, item26, item27, item28, item29, item30, item31, item32, item33, item34, item35, item36,
		item37, item38, item39, item40]
	}
	
}
