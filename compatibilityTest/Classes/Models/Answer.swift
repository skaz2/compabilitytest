//
//  Answer.swift
//  compatibilityTest
//
//  Created by Hadevs on 04/04/2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation

class Answer: Codable {
	var number: Int
	
	init(number: Int) {
		self.number = number
	}
}
