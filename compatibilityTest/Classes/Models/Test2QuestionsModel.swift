//
//  Test1.swift
//  compatibilityTest
//
//  Created by spbiphones on 24.02.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import UIKit

struct Test2QuestionsModel {
	var question1: String
	var question2: String
	var question3: String
	var question4: String
	var question5: String
	var question6: String
	//	let myDictionary = ["Hello": [1, 2, 3], "World": [4, 5, 6]]
	
	static func fetchTest2() -> [Test2QuestionsModel] {
		let item1 = Test2QuestionsModel(question1: "Нет", question2: "Крайне редко", question3: "Да, но в меру", question4: "Борюсь со своей ленью", question5: "Ленивец – моё тотемное животное!", question6: "Другой ответ")
		
		let item2 = Test2QuestionsModel(question1: "Грубость", question2: "Правдивость", question3: "Тактичность", question4: "Лживость", question5: "Замкнутость", question6: "Дружелюбие")
		
		let item3 = Test2QuestionsModel(question1: "Добросовестность", question2: "Ответственность", question3: "Трудолюбие", question4: "Леность", question5: "Доведение дел до конца", question6: "Откладывание дел")
		
		let item4 = Test2QuestionsModel(question1: "Самовлюбленность", question2: "Самокритичность", question3: "Скромность", question4: "Гордость", question5: "Самоуверенность", question6: "Самоутверждение")
		
		let item5 = Test2QuestionsModel(question1: "Жадность", question2: "Бережливость", question3: "Щедрость", question4: "Расточительность", question5: "Неряшливость", question6: "Аккуратность")
		
		let item6 = Test2QuestionsModel(question1: "Менее 6", question2: "7-8", question3: "9 и более", question4: "График не стабильный, всегда по-разному", question5: "Не знаю", question6: "Другой ответ")
		
		let item7 = Test2QuestionsModel(question1: "Да, очень", question2: "Да, но стараюсь меньше это показывать", question3: "Нет, это чувство для людей с комплексами", question4: "Только если есть реальный повод", question5: "Только если нет доверия к человеку", question6: "Другой ответ")
		
		let item8 = Test2QuestionsModel(question1: "Причин может быть много", question2: "Ложью и неискренностью", question3: "Бывает люди просто раздражают и бесят", question4: "Навязыванием своего мнения", question5: "Не справедливостью и хитростью", question6: "Другой ответ")
		
		let item9 = Test2QuestionsModel(question1: "Каждый день и очень сильно", question2: "Очень редко", question3: "Когда сталкиваюсь в очередной раз с тем, что мне не нравится и мое терпение не выдерживает", question4: "Часто, но не сильно", question5: "Для меня злость как самовыражение так как по-другому не понимают", question6: "Другой ответ")
		
		let item10 = Test2QuestionsModel(question1: "Да и мне нравилось", question2: "К сожалению, да", question3: "Редко", question4: "Не приходилось", question5: "До сих пор иногда бью людей", question6: "Другой ответ")
		
		let item11 = Test2QuestionsModel(question1: "Да, часто", question2: "Редко", question3: "Я сам обижал первым что бы меня не обижали", question4: "Нет, со мной только дружили", question5: "До сих пор обижают", question6: "Другой ответ")
		
		let item12 = Test2QuestionsModel(question1: "Да, не заботились обо мне, не уделяли внимания", question2: "Да, били и унижали", question3: "Не обижали", question4: "Да, и я со временем мстил в ответ", question5: "В основном заставляли много работать", question6: "Другой ответ")
		
		let item13 = Test2QuestionsModel(question1: "Часто", question2: "Всегда, без этого никак", question3: "Бывает, но очень редко", question4: "Никогда", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item14 = Test2QuestionsModel(question1: "Да я очень внимательный и наблюдательный", question2: "Если мне тема обсуждения интересна, то да", question3: "Очень редко бываю внимательным", question4: "Только делаю вид что внимательно слушаю", question5: "Мне не интересно слушать долго людей", question6: "Другой ответ")
		
		let item15 = Test2QuestionsModel(question1: "Абсурдный", question2: "Импровизация", question3: "Сарказм", question4: "Черный юмор", question5: "Люблю разносторонний юмор", question6: "Другой ответ")
		
		let item16 = Test2QuestionsModel(question1: "К сожалению, слишком добрый", question2: "Добрый, но в меру и не со всеми", question3: "Я скорее всего справедливый", question4: "Добрый к людям, но к себе злой", question5: "Да, часто помогаю людям", question6: "Не считаю себя добрым это слабость")
		
		let item17 = Test2QuestionsModel(question1: "Лживость и скрытность", question2: "Лень и безграмотность", question3: "Лицемерие и подхалимство", question4: "Гордыня и завистливость", question5: "Все эти качества не допустимы у нормального человека", question6: "Другой ответ")
		
		let item18 = Test2QuestionsModel(question1: "Да очень", question2: "Бывает, но редко", question3: "Есть отдельные категории тем, когда я сентиментальный, а так нет", question4: "Нет", question5: "Другой ответ", question6: "")
		
		let item19 = Test2QuestionsModel(question1: "Когда подшучивают надо мной при других людях", question2: "Когда меня не слушают", question3: "Когда обесценивают мои действия", question4: "Когда меня оскорбляют", question5: "Когда меня критикуют", question6: "Другой ответ")
		
		let item20 = Test2QuestionsModel(question1: "Да конечно", question2: "Мне всё равно где жить", question3: "Если я не буду привязан к месту работой и делами", question4: "Думаю, нет", question5: "Смотря в какую страну", question6: "Другой ответ")
		
		let item21 = Test2QuestionsModel(question1: "Карьера", question2: "Семья", question3: "Здоровье", question4: "Интеллектуальное развитие", question5: "Духовное развитие", question6: "Другой ответ")
		
		let item22 = Test2QuestionsModel(question1: "Да, злюсь и стою на своём", question2: "Немного расстроюсь, но признаюсь в том, что не прав", question3: "Буду спорить пока не закончатся аргументы", question4: "Нет, истина превыше всего", question5: "Когда как", question6: "Другой ответ")
		
		let item23 = Test2QuestionsModel(question1: "Да", question2: "Если не по работе или важному делу, то да", question3: "Иногда", question4: "Нет", question5: "Я вообще не обращаю на это внимания", question6: "Другой ответ")
		
		let item24 = Test2QuestionsModel(question1: "Люблю поиграть в ПК игры", question2: "Играю на приставке", question3: "Смотрю в основном фильмы", question4: "Смотрю телевизор", question5: "Игры в телефонах и планшетах", question6: "Никакие из перечисленных")
		
		let item25 = Test2QuestionsModel(question1: "Наука и техника", question2: "Жизнь артистов и звёзд", question3: "Законы и политика", question4: "Жизнь в других странах", question5: "Мне интересно практически всё", question6: "Другой ответ")
		
		let item26 = Test2QuestionsModel(question1: "Да смог бы", question2: "Если со своим нынешним партнёром, то да", question3: "Думаю, нет", question4: "Только если за деньги", question5: "Другой ответ", question6: "")
		
		let item27 = Test2QuestionsModel(question1: "Отлично, у меня их два и более", question2: "Хорошо, есть один любимчик", question3: "Надоедает ухаживать за ними", question4: "Плохо", question5: "Люблю, но заводить не буду", question6: "Другой ответ")
		
		let item28 = Test2QuestionsModel(question1: "Отлично, сам такой", question2: "Нормально, это дело каждого", question3: "Не очень", question4: "Если не навязывают свои предпочтения, то я спокоен", question5: "Смотря, какая религия", question6: "Другой ответ")
		
		let item29 = Test2QuestionsModel(question1: "В основном то, что популярно", question2: "Тяжелую музыку ", question3: "Реп, андеграунд", question4: "Классика и джаз", question5: "Я меломан слушаю очень разные направления", question6: "Другой ответ")
		
		let item30 = Test2QuestionsModel(question1: "Да, при любой удобной возможности", question2: "Если он будет её слушать при мне, то да", question3: "Нет, никогда", question4: "Буду ему мстить и включать то, что не нравится ему", question5: "Буду тоже с ним слушать пока не пойму почему она ему нравится", question6: "Другой ответ")
		
		let item31 = Test2QuestionsModel(question1: "Да, я всегда стараюсь улучшить свою жизнь", question2: "Когда как, чего-то не хватает", question3: "Не знаю", question4: "Не очень", question5: "Я сейчас несчастен", question6: "Другой ответ")
		
		let item32 = Test2QuestionsModel(question1: "Думаю, я", question2: "Мы оба ", question3: "Не понимаю вопрос", question4: "Мы оба ещё развиваемся", question5: "Мой партнёр более развит в этом", question6: "Другой ответ")
		
		let item33 = Test2QuestionsModel(question1: "Часто, считаю у нас много разных взглядов на жизнь", question2: "Иногда бывает, но приходим к общему мнению", question3: "Редко, обычно у нас схожие мнения", question4: "Не помню", question5: "Другой ответ", question6: "")
		
		let item34 = Test2QuestionsModel(question1: "Да, что бы доказать свою правоту", question2: "Да, но ради истины, я всегда готов менять свое мнение", question3: "Мне всё равно какая цель, лишь бы поспорить", question4: "Не люблю спорить", question5: "Другой ответ", question6: "")
		
		let item35 = Test2QuestionsModel(question1: "Конечно", question2: "Да, но в основном с целью дополнительных знаний", question3: "Да, в основном нравственное совершенствование", question4: "Да, мне интересен смысл человеческого предназначения", question5: "Мне кажется, это всё какой-то бред", question6: "Другой ответ")
		
		let item36 = Test2QuestionsModel(question1: "Да", question2: "Очень редко", question3: "Пробовал, не понял смысл", question4: "Нет", question5: "Не понимаю о чём речь", question6: "Другой ответ")
		
		let item37 = Test2QuestionsModel(question1: "Да, часто", question2: "Иногда", question3: "Бывает, но редко", question4: "Больше года этого не делал", question5: "Очень давно это не делаю", question6: "Другой ответ")
		
		let item38 = Test2QuestionsModel(question1: "Конечно, это нормально", question2: "Если не часто, то да", question3: "Нет", question4: "Мастурбация для меня почти как измена", question5: "Можно, если в тайне от партнёра", question6: "Другой ответ")
		
		let item39 = Test2QuestionsModel(question1: "Мышление и стремление к развитию, стараюсь найти таких же людей или лучше", question2: "Ничего, считаю, что я такой же, как и большинство", question3: "Люблю неформально выглядеть и говорить", question4: "Практически всё отличает меня от большинства", question5: "Те, кто считают, что отличаются от большинства людей всего лишь люди с кучей комплексов", question6: "Другой ответ")
		
		let item40 = Test2QuestionsModel(question1: "Да и очень много", question2: "У всех они есть, просто все их пытаются скрыть", question3: "От комплексов, которые мне мешали жить, я избавился", question4: "Нет и не было", question5: "Затрудняюсь ответить", question6: "Другой ответ")
		
		let item41 = Test2QuestionsModel(question1: "Да", question2: "Если предсказания хорошие, то да", question3: "Отношусь скептически", question4: "Не верю, но раньше верил", question5: "Никогда в это не верил", question6: "Другой ответ")
		
		let item42 = Test2QuestionsModel(question1: "Да сильно, так как я очень брезгливый и аккуратный человек", question2: "Да, постоянно буду ему на это указывать", question3: "Смотря, какая степень неряшливости и неаккуратности", question4: "Думаю, нет", question5: "Я сам такой поэтому мне всё равно", question6: "Другой ответ")
		
		let item43 = Test2QuestionsModel(question1: "Да", question2: "Нет", question3: "", question4: "", question5: "", question6: "")
		
		let item44 = Test2QuestionsModel(question1: "Почти всё время, общение развлечения", question2: "Много, но в основном с пользой, для поиска информации", question3: "Интернет одно из лучших изобретений человечества там можно", question4: "Редко, только мессенджеры в телефоне", question5: "Вообще не пользуюсь интернетом", question6: "Другой ответ")
		
		let item45 = Test2QuestionsModel(question1: "У меня нет друзей, только знакомые", question2: "Один друг", question3: "Много", question4: "Несколько лучших друзей", question5: "Я вообще не общительный и друзей нет", question6: "Нет, но хотелось бы")

		
		
		return [item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, item14, item15, item16, item17, item18, item19, item20, item21, item22, item23, item24, item25, item26, item27, item28, item29, item30, item31, item32, item33, item34, item35, item36, item37, item38, item39, item40, item41, item42, item43, item44, item45]
	}
	
}
