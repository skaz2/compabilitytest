//
//  FirebaseResult.swift
//  compatibilityTest
//
//  Created by spbiphones on 26.01.2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation

enum FirebaseResult {
	case success
	case error(String)
}
