//
//  Extensions+Array.swift
//  compatibilityTest
//
//  Created by Hadevs on 05/04/2019.
//  Copyright © 2019 skaz. All rights reserved.
//

import Foundation

extension Array where Element == Int {
	func countOfEqualElements(with array: [Int]) -> Int {
		var counter = 0
		let combined = self + array
		var duplicates: [Int] = []
		for element in combined {
			if duplicates.contains(element) {
				counter += 1
			} else {
				duplicates.append(element)
			}
		}
		return counter
	}
}
